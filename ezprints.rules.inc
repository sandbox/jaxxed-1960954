<?php
/**
* @file ezprints interface rules interface
*
*/


/**
* Implementation of hook_rules_event_info().
* @ingroup rules
*/
function ezprints_rules_event_info() {
  return array(
    'ezprints_fulfillment_submit_pre' => array(
      'group' => t('EZPrint'),
      'label' => t('An EZPrints fullfilment order is about to be submitted'),
      'module' => 'ezprints',
      'arguments' => array(
        'account' => array('type' => 'user', 'label' => t('The user on whose behalf the order was submitted.')),
        'order' => array('type' => 'commerce_order', 'label' => t('Order.')),
        'orderXML' => array('type' => 'string', 'label' => t('Order XML to be submitted.')),
      ),
    ),
    'ezprints_fulfillment_submit_success' => array(
      'group' => t('EZPrint'),
      'label' => t('An EZPrints fullfilment order was submitted'),
      'module' => 'ezprints',
      'arguments' => array(
        'account' => array('type' => 'user', 'label' => t('The user on whose behalf the order was submitted.')),
        'order' => array('type' => 'commerce_order', 'label' => t('Order.')),
        'response' => array('type' => 'simplexml', 'label' => t('Order response XML.')),
      ),
    ),
    'ezprints_fulfillment_submit_error' => array(
      'group' => t('EZPrint'),
      'label' => t('An EZPrints fullfilment order could\'t be submitted properly.'),
      'module' => 'ezprints',
      'arguments' => array(
        'account' => array('type' => 'user', 'label' => t('The user on whose behalf the order was submitted.')),
        'order' => array('type' => 'commerce_order', 'label' => t('Order.')),
        'message' => array('type' => 'string', 'label' => t('Order error message.')),
      ),
    ),
  );
}

/**
 * Implementation of hook_rules_action_info().
 */
function ezprints_rules_action_info() {
  return array(
    'ezprints_action_order_fulfill' => array(
      'label' => t('Turn a commerce order into an EZPrints fullfillment order and submit it.'),
      'arguments' => array(
        'order' => array('type' => 'commerce_order', 'label' => t('The order object.')),
      ),
      'provides' => array(
        'report' => array('type' => 'ezprints_response', 'label'=> t('EZPrint response status object.')),
      ),
      'module' => 'ezprints',
    ),
  );
}

/**
* Rules Action handlers
*/

/**
* EZPrint fullfillment handler
*/
function ezprints_action_order_fulfill($order) {
  return array('response'=>ezprints_fulfill($order));
}


/**
 * Implementation of hook_rules_data_type_info().
 */
// function ezprints_rules_data_type_info() {
//   return array(
//     'ezprints_response' => array(
//       'label' => t('EZPrints Response'),
//       'class' => 'ezprint_status',
//       'savable' => FALSE,
//       'identifiable' => TRUE,
//       'uses_input_form' => FALSE,
//       'module' => 'ezprints',
//       'file'
//     ),
//   );
// }

