<?php
/**
* @file ezprints admin interface
*
*/

/**
* Admin settings form
*/
function ezprints_admin_form($form_id,&$form_state) {

  $form = array();

  list($connection,$contract) = _ezprints_settings();

  $form['ezprints_connection'] = array(
    '#type' => 'fieldset',
    '#title' => t('EZPrints Connection information'),
    '#description' => t('This information is required, and is used to submit requests to the EZPrints interface API.'),
    '#tree' => true,

    'url' => array(
      '#type' => 'textfield',
      '#title' => t('Login URL'),
      '#description' => t('URL to the EZPrints login handler'),
      '#default_value' => isset($connection['url'])?$connection['url']:''
    ),
    'user' => array(
      '#type' => 'textfield',
      '#title' => t('Login user name'),
      '#description' => t('user for the EZPrints login handler'),
      '#default_value' => isset($connection['user'])?$connection['user']:''
    ),
    'password' => array(
      '#type' => 'textfield',
      '#title' => t('Password'),
      '#description' => t('password for user'),
      '#default_value' => isset($connection['password'])?$connection['password']:''
    ),

  );

  $form['ezprints_account'] = array(
    '#type' => 'fieldset',
    '#title' => t('EZPrints Account'),
    '#description' => t('This information is required, and is used to login to the EZPrints interface API, and to associate jobs to the account.'),
    '#tree' => true,

    'partnerID' => array(
      '#type' => 'textfield',
      '#title' => t('Partner ID'),
      '#description' => t(''),
      '#default_value' => isset($contract['partnerID'])?$contract['partnerID']:''
    ),
    'DeploymentKey' => array(
      '#type' => 'textfield',
      '#title' => t('Deployment Key'),
      '#description' => t(''),
      '#default_value' => isset($contract['deploymentKey'])?$contract['deploymentKey']:''
    ),
  );

  $form = system_settings_form($form);

  // We don't want to call system_settings_form_submit(), so change #submit.
//   array_pop($form['#submit']);
//   $form['#submit'][] = 'ezprints_admin_form_submit';
//   $form['#validate'][] = 'ezprints_admin_form_validate';

  return $form;
}