<?php
/**
* @file ezprints module helper functions and classes
*
*/

/**
* EXPRINTS ORDER STATUS OBJECT
*/
class ezprint_status {

  private $status;
  private $message;
  private $response;

  static public function get($response) {
    $class = __class__;
    return new $class($response);
  }

  private function __construct($response) {
    $this->response = $response;
    $this->status = self::statusFromResponse($response);
    $this->message = self::messageFromResponse($response);
  }

  public function status() {
    return $this->status;
  }
  public function message() {
    return $this->message();
  }

  public function __toString() {
    return $this->message();
  }

  static private function statusFromResponse($response) {

  }
  static private function messageFromResponse($response) {

  }

}

/**
* Build an EZPrints Order (XML object) from a Commerce order
*
* @param $order Commerce order object or order_id
* @returns SimpleXML or DOM XML object
*
* @see _ezprints_validate
*
* @throws may pass a validation failure exception down
*/
function _ezprints_convertOrderForEZPrint($order) {
  $order = _ezprints_validate($order);

  if ( empty($order) ) {
    return;
  }

  /**
  * turn an order into an XML object
  */
  $xml = new SimpleXML();

  // ....

  return $xml;
}


/**
* Retrieve EZPRINTS configuration from DB
*
* @throws InvalidArgumentException if the configuration
* hasn't been done yet
*/
function _ezprints_settings() {
  $connection = variable_get('ezprints_connection',array(
    'url' => 'http://tools.ezpservices.com/Login.aspx',
    'user' => 'paul@printense.com',
    'password' => 'welcome'
  ));
  $contract = variable_get('ezprints_contract',array(
    'partnerID' => 1025,
    'deploymentKey' => '3af2a60b-3a86-45a8-bfb6-a860909db625'
  ));

  // sanity test
  if (empty($connection) || empty($contract)) {
    throw new InvalidArgumentException('Missing EZprints configuration.  EXPrints must be configured before it can be used.',PRINTENSE_EZPRINTS_ARGUMENT_SETTINGS);
  }

  return array($connection,$contract);
}

/**
* Check and if necessary, load an order
*/
function _ezprints_validate($order) {

}

/**
* Convert an XML object to XML string
*
* @Note necessary as we haven't yet decided on simpleXML versus DOM
*
* @throws InvalidArgumentException if the xml object can't be converted
*/
function _ezprints_XMLtoString($XML) {

}
